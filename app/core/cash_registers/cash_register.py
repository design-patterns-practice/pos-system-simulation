from typing import Dict, Iterable, Optional, Protocol

from core.items import Item
from core.payments import IPayment
from core.prices import IPriceCalculator
from core.printers import IPrinter
from core.receipts import IReceipt, Receipt


class ICashRegister(Protocol):
    def open_receipt(self) -> None:
        """Opens a receipt for a new customer."""

    def process_items(self, items: Iterable[Item], /) -> None:
        """Adds all of the specified items to the currently active receipt."""

    def print_receipt(self, *, printer: IPrinter[IReceipt]) -> None:
        """Prints the currently active receipt using the specified receipt printer."""

    def accept_payment(self, payment: IPayment, /) -> None:
        """Accepts the payment of the specified form."""

    def close_receipt(self) -> IReceipt:
        """Closes the currently active receipt and returns it."""


class CashRegister:  # Receipt Interactor
    def __init__(self, *, price_calculator: IPriceCalculator) -> None:
        self.__receipt: Optional[Receipt] = None
        self.__payment_received = False
        self.__price_calculator = price_calculator

    def open_receipt(self) -> None:
        """Opens a receipt for a new customer."""
        assert (
            self.__receipt is None
        ), f"An active receipt is already open for a customer. The Receipt: {self.__receipt}"

        self.__receipt = Receipt()
        self.__payment_received = False

    def process_items(self, items: Iterable[Item], /) -> None:
        """Adds all of the specified items to the currently active receipt."""
        assert (
            self.__receipt is not None
        ), "Cannot process items without an active open receipt."

        item_counts: Dict[Item, int] = {}

        for item in items:
            item_counts.setdefault(item, 0)
            item_counts[item] += 1

        for item in items:
            self.__receipt.add_item(
                item,
                price=self.__price_calculator.price_of(item, context_items=item_counts),
            )

    def print_receipt(self, *, printer: IPrinter[IReceipt]) -> None:
        """Prints the currently active receipt using the specified receipt printer."""
        assert (
            self.__receipt is not None
        ), "Cannot print the receipt as there is no active one."

        printer.print(self.__receipt)

    def accept_payment(self, payment: IPayment, /) -> None:
        """Accepts the payment of the specified form."""
        assert (
            self.__receipt is not None
        ), "Cannot receive payment as there is no active receipt."

        payment.transfer(self.__receipt.get_total_cost())
        self.__payment_received = True

    def close_receipt(self) -> IReceipt:
        """Closes the currently active receipt and returns it."""
        assert (
            self.__receipt is not None
        ), "Cannot close the receipt as there is no active one."

        assert (
            self.__payment_received
        ), "Cannot close a receipt that has not been paid for."

        receipt = self.__receipt
        self.__receipt = None

        return receipt
