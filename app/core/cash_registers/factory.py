from typing import Protocol

from core.cash_registers.cash_register import CashRegister, ICashRegister
from core.data_bases import IDataBase
from core.prices import PriceCalculator


class ICashRegisterFactory(Protocol):
    def create_cash_register(self, *, data_base: IDataBase) -> ICashRegister:
        """Creates a cash register."""


class CashRegisterFactory:
    @staticmethod
    def create_cash_register(*, data_base: IDataBase) -> ICashRegister:
        """Creates a cash register."""
        return CashRegister(price_calculator=PriceCalculator(data_base=data_base))
