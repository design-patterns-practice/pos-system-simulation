from dataclasses import dataclass
from typing import List

from core.items import Item
from core.payments import IPayment


@dataclass
class Customer:
    id: str
    item_list: List[Item]
    payment: IPayment
