from random import choice, randint
from typing import List, Protocol
from uuid import uuid4

from core.customers.customer import Customer
from core.items import Item
from core.payments import IPayment


class ICustomerFactory(Protocol):
    def next_customer(self) -> Customer:
        """Generates the next customer to be served."""


class RandomCustomerFactory:
    def __init__(
        self,
        *,
        possible_items: List[Item],
        payment_methods: List[IPayment],
        min_items: int = 5,
        max_items: int = 10
    ):
        """Creates a random customer factory. The customers will have randomly chosen items from the specified
        possible item list."""
        assert (
            len(possible_items) >= 0
        ), "You should specify at least one possible item to choose from."
        assert (
            len(payment_methods) >= 0
        ), "You should specify at least one possible payment method to choose from."
        assert (
            min_items >= 0
        ), "The minimum size for the item list must not be a negative number."
        assert (
            min_items < max_items
        ), "The minimum size of the item list should be less than the maximum."

        self.__possible_items = possible_items
        self.__payment_methods = payment_methods
        self.__min_items = min_items
        self.__max_items = max_items

    def next_customer(self) -> Customer:
        """Generates the next customer to be served."""
        customer_id = str(uuid4())
        items = [
            choice(self.__possible_items)
            for _ in range(randint(self.__min_items, self.__max_items))
        ]
        payment = choice(self.__payment_methods)

        return Customer(id=customer_id, item_list=items, payment=payment)
