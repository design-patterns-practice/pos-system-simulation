from typing import List, Protocol

from core.items import IDiscount, Item


class IDataBase(Protocol):
    def get_item_price(self, item: Item) -> float:
        """Returns the total_price of the specified item from the data base."""

    def get_item_discounts(self, item: Item) -> List[IDiscount]:
        """Returns a list of all discounts related to the specified item."""

    def add_sold_item(
        self, item: Item, /, *, total_price: float, count: int = 1
    ) -> None:
        """Adds the the given item of the specified amount to the data base's sold item data."""

    def get_sold_item_count(self, item: Item) -> int:
        """Returns the count of units sold for the specified item."""

    def get_distinct_sold_items(self) -> List[Item]:
        """Returns a list of all distinct items sold."""

    def get_total_revenue(self) -> float:
        """Returns the total revenue earned from all sold items."""

    def clear_sold_items(self) -> None:
        """Clears all data about sold items."""
