from dataclasses import dataclass
from typing import Dict, Protocol

from core.items.item import Item


class IDiscount(Protocol):
    def get_related_item(self) -> Item:
        """Returns the item that this discount is related to."""

    def applied_to(self, item: Item, /, *, context_items: Dict[Item, int]) -> float:
        """Calculates the discount that applies to specified item if the consumer
        wishes to buy the other products as well.

        :returns The discount percentage (0 if it does not apply)."""


@dataclass(frozen=True)
class Discount:
    related_item: Item
    required_items: Dict[Item, int]
    reduction_percentage: float

    def get_related_item(self) -> Item:
        """Returns the item that this discount is related to."""
        return self.related_item

    def applied_to(self, item: Item, /, *, context_items: Dict[Item, int]) -> float:
        """Calculates the discount that applies to specified item if the consumer
        wishes to buy the other products as well.

        :returns The discount percentage (0 if it does not apply)."""
        return (
            self.reduction_percentage
            if item == self.related_item
            and all(
                context_items.get(required_item, 0) >= count
                for required_item, count in self.required_items.items()
            )
            else 0
        )
