from typing import Protocol


class IPayment(Protocol):
    def transfer(self, amount: float, /) -> None:
        """Executes this payment by transferring the specified amount."""
