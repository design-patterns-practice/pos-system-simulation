from typing import Dict, Protocol

from core.data_bases import IDataBase
from core.items import Item

ContextItems = Dict[Item, int]


class IPriceCalculator(Protocol):
    def price_of(self, item: Item, /, *, context_items: ContextItems) -> float:
        """Returns the calculated total_price for the specified item including applicable discounts."""


class PriceCalculator:
    def __init__(self, *, data_base: IDataBase) -> None:
        self.__data_base = data_base

    def price_of(self, item: Item, /, *, context_items: ContextItems) -> float:
        """Returns the calculated total_price for the specified item including applicable discounts."""
        reductions = (
            discount.applied_to(item, context_items=context_items)
            for discount in self.__data_base.get_item_discounts(item)
        )

        total_reduction = sum(
            reduction for reduction in reductions if reduction is not None
        )

        if total_reduction > 100:
            total_reduction = 100

        return self.__data_base.get_item_price(item) * (100 - total_reduction) / 100
