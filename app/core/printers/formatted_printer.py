from abc import ABC, abstractmethod
from typing import Any, Callable, Dict, Generic, List, TypeVar

from core.printers.formatter import IDataFormatter, TabularFormatter


def _default_key(row: "AbstractFormattedPrinter.Row") -> Any:
    return row[next(iter(row))] if len(row) > 0 else 0


T = TypeVar("T")


class AbstractFormattedPrinter(ABC, Generic[T]):
    Row = Dict[str, Any]
    OutputStream = Callable[[str], None]
    OrderKey = Callable[[Row], Any]

    def __init__(
        self,
        *,
        output_stream: OutputStream = print,
        formatter: IDataFormatter = TabularFormatter(),
        order_key: OrderKey = _default_key,
    ) -> None:
        self.__output_stream = output_stream
        self.__formatter = formatter
        self.__order_key = order_key

    def print(self, data: T, /) -> None:
        """Prints the specified receipt."""
        header = self._get_header(data)

        ordered_rows = [
            [row[column] for column in header]
            for row in sorted(self._get_rows(data), key=self.__order_key)
        ]

        self.__output_stream(
            self.__formatter.format(
                header=header,
                rows=ordered_rows,
                summary=self._get_summary(data),
            )
        )

    @abstractmethod
    def _get_header(self, data: T, /) -> List[str]:
        """Returns the header to be used when printing the data."""

    @abstractmethod
    def _get_rows(self, data: T, /) -> List[Row]:
        """Returns the rows to be included in the printed data."""

    @abstractmethod
    def _get_summary(self, data: T, /) -> str:
        """Returns the summary to be appended at the end of the printed data."""
