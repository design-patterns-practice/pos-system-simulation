from typing import Any, List, Protocol

from tabulate import tabulate

HeaderRow = List[str]
ReportRow = List[Any]


class IDataFormatter(Protocol):
    def format(self, *, header: HeaderRow, rows: List[ReportRow], summary: str) -> str:
        """Formats the rows of the specified data and returns it as a string."""


class TabularFormatter:
    def __init__(
        self, *, table_format: str = "pipe", float_format: str = ".2f"
    ) -> None:
        self.__table_format = table_format
        self.__float_format = float_format

    def format(self, *, header: HeaderRow, rows: List[ReportRow], summary: str) -> str:
        """Formats the rows of the specified data and returns it as a string."""

        table = tabulate(
            tabular_data=rows,
            headers=header,
            tablefmt=self.__table_format,
            floatfmt=self.__float_format,
        )

        return f"{table}\n" f"{summary}"
