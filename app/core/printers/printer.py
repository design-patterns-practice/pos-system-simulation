from typing import Protocol, TypeVar

T = TypeVar("T", contravariant=True)


class IPrinter(Protocol[T]):
    def print(self, data: T, /) -> None:
        """Prints the specified data."""
