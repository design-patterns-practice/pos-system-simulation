from typing import Final, List

from core.printers.formatted_printer import AbstractFormattedPrinter
from core.receipts import IReceipt


class ReceiptPrinter(AbstractFormattedPrinter[IReceipt]):
    NAME_COLUMN: Final[str] = "Name"
    UNITS_COLUMN: Final[str] = "Units"
    PRICE_COLUMN: Final[str] = "Price"
    TOTAL_COLUMN: Final[str] = "Total"

    def _get_header(self, receipt: IReceipt, /) -> List[str]:
        """Returns the header to be used when printing the data."""
        return [
            ReceiptPrinter.NAME_COLUMN,
            ReceiptPrinter.UNITS_COLUMN,
            ReceiptPrinter.PRICE_COLUMN,
            ReceiptPrinter.TOTAL_COLUMN,
        ]

    def _get_rows(self, receipt: IReceipt, /) -> List[AbstractFormattedPrinter.Row]:
        """Returns the rows to be included in the printed data."""
        return [
            {
                ReceiptPrinter.NAME_COLUMN: item,
                ReceiptPrinter.UNITS_COLUMN: receipt.get_item_count(item),
                ReceiptPrinter.PRICE_COLUMN: receipt.get_item_cost(item),
                ReceiptPrinter.TOTAL_COLUMN: receipt.get_item_total_cost(item),
            }
            for item in receipt.get_distinct_items()
        ]

    def _get_summary(self, receipt: IReceipt, /) -> str:
        """Returns the summary to be appended at the end of the printed data."""
        return f"Sum: {receipt.get_total_cost():.2f}"
