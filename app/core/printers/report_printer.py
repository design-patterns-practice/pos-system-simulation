from typing import Final, List

from core.printers.formatted_printer import AbstractFormattedPrinter
from core.reports import IReport


class ReportPrinter(AbstractFormattedPrinter[IReport]):
    NAME_COLUMN: Final[str] = "Name"
    UNITS_COLUMN: Final[str] = "Sold"

    def _get_header(self, report: IReport, /) -> List[str]:
        """Returns the header to be used when printing the data."""
        return [ReportPrinter.NAME_COLUMN, ReportPrinter.UNITS_COLUMN]

    def _get_rows(self, report: IReport, /) -> List[AbstractFormattedPrinter.Row]:
        """Returns the rows to be included in the printed data."""
        return [
            {
                ReportPrinter.NAME_COLUMN: item,
                ReportPrinter.UNITS_COLUMN: report.get_sold_item_count(item),
            }
            for item in report.get_distinct_sold_items()
        ]

    def _get_summary(self, report: IReport, /) -> str:
        """Returns the summary to be appended at the end of the printed data."""
        return f"Total Revenue: {report.get_total_revenue():.2f}"
