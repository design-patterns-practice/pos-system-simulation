from core.data_bases import IDataBase
from core.receipts.handlers.handler import ReceiptHandler
from core.receipts.receipt import IReceipt


class ReceiptDataBaseHandler(ReceiptHandler):
    def __init__(self, *, data_base: IDataBase) -> None:
        super().__init__()
        self.__data_base = data_base

    def handle(self, receipt: IReceipt, /) -> None:
        """Handles the specified receipt and potentially passes it to the next handler in the chain."""
        for item in receipt.get_distinct_items():
            item_units = receipt.get_item_count(item)
            item_total_cost = receipt.get_item_total_cost(item)

            self.__data_base.add_sold_item(
                item, total_price=item_total_cost, count=item_units
            )

        self._to_next(receipt)
