from abc import ABC, abstractmethod
from typing import Optional, Protocol

from core.receipts.receipt import IReceipt


class IReceiptHandler(Protocol):
    def handle(self, receipt: IReceipt, /) -> None:
        """Handles the specified receipt and potentially passes it to the next handler in the chain."""

    def chain(self, handler: "IReceiptHandler", /) -> "IReceiptHandler":
        """Chains this handler with the specified one and returns itself (useful for a flowing interface)."""


class ReceiptHandler(ABC):
    def __init__(self) -> None:
        self.__next: Optional[IReceiptHandler] = None

    @abstractmethod
    def handle(self, receipt: IReceipt, /) -> None:
        """Handles the specified receipt and potentially passes it to the next handler in the chain."""

    def chain(self, handler: "IReceiptHandler", /) -> "IReceiptHandler":
        """Chains this handler with the specified one and returns itself (useful for a flowing interface)."""
        if self.__next is None:
            self.__next = handler
        else:
            self.__next.chain(handler)

        return self

    def _to_next(self, receipt: IReceipt) -> None:
        if self.__next is not None:
            self.__next.handle(receipt)
