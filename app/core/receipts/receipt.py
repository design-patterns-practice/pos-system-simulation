from dataclasses import dataclass
from typing import List, Protocol, Tuple

from core.items import Item


class IReceipt(Protocol):
    def get_distinct_items(self) -> Tuple[Item, ...]:
        """Returns a list of distinct items added to this receipt."""

    def get_item_count(self, item: Item) -> int:
        """Returns the number times the specified item was added to this receipt."""

    def get_item_cost(self, item: Item) -> float:
        """Returns the (average) cost of a single unit of the specified item in the receipt."""

    def get_item_total_cost(self, item: Item) -> float:
        """Returns the accumulated cost of all items of the specified type added to this receipt."""

    def add_item(self, item: Item, /, *, price: float) -> None:
        """Adds the specified item to this receipt."""

    def get_total_cost(self) -> float:
        """Returns the total cost of the items included in this receipt."""


class Receipt:
    @dataclass(frozen=True)
    class _PricedItem:
        item: Item
        price: float

    def __init__(self) -> None:
        self.__items: List[Receipt._PricedItem] = []

    def get_distinct_items(self) -> Tuple[Item, ...]:
        """Returns a list of distinct items added to this receipt."""
        return tuple({priced_item.item for priced_item in self.__items})

    def get_item_count(self, item: Item) -> int:
        """Returns the number of times the specified item was added to this receipt."""
        count = sum(1 for priced_item in self.__items if priced_item.item == item)

        assert count > 0, f"The item {item} is not included in this receipt."

        return count

    def get_item_cost(self, item: Item) -> float:
        """Returns the (average) cost of a single unit of the specified item in the receipt."""
        return self.get_item_total_cost(item) / self.get_item_count(item)

    def get_item_total_cost(self, item: Item) -> float:
        """Returns the accumulated cost of all items of the specified type added to this receipt."""
        cost = sum(
            priced_item.price
            for priced_item in self.__items
            if priced_item.item == item
        )

        assert cost > 0, f"The item {item} is not included in this receipt."

        return cost

    def add_item(self, item: Item, /, *, price: float) -> None:
        """Adds the specified item to this receipt."""
        self.__items.append(Receipt._PricedItem(item, price))

    def get_total_cost(self) -> float:
        """Returns the total cost of the items included in this receipt."""
        return sum([priced_item.price for priced_item in self.__items])
