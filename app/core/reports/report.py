from typing import List, Protocol

from core.items import Item


class IReport(Protocol):
    def get_sold_item_count(self, item: Item) -> int:
        """Returns the count of units sold for the specified item."""

    def get_distinct_sold_items(self) -> List[Item]:
        """Returns a list of all distinct items sold."""

    def get_total_revenue(self) -> float:
        """Returns the total revenue earned from all sold items."""
