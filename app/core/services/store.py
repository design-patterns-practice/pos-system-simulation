from typing import Optional

from core.cash_registers import ICashRegisterFactory
from core.customers import Customer
from core.data_bases import IDataBase
from core.printers import IPrinter, ReceiptPrinter
from core.receipts import IReceipt, IReceiptHandler, ReceiptDataBaseHandler
from core.reports import IReport


class StoreService:
    IReceiptPrinter = IPrinter[IReceipt]
    IReportPrinter = IPrinter[IReport]

    def __init__(
        self,
        *,
        cash_registers: int,
        daily_shifts: int,
        cash_register_factory: ICashRegisterFactory,
        data_base: IDataBase,
        receipt_handler: Optional[IReceiptHandler] = None,
    ) -> None:
        assert (
            cash_registers > 0
        ), "The number of cash registers must be a positive number."
        assert (
            daily_shifts > 0
        ), "The number of shifts per day must be a positive number."

        self.__daily_shifts = daily_shifts
        self.__remaining_shifts = 0
        self.__data_base = data_base
        self.__cash_register = cash_register_factory.create_cash_register(
            data_base=data_base
        )
        self.__receipt_handler = receipt_handler or self.__get_default_handler()

    def open_store(self, *, clear_data: bool = False) -> None:
        """Opens the store. This needs to be invoked before the store can serve any customers or if all shifts in
        a day have ended (the store has closed)."""
        assert self.__remaining_shifts == 0, "Cannot open an already opened store."

        if clear_data:
            self.__data_base.clear_sold_items()

        self.__remaining_shifts = self.__daily_shifts

    def serve_customer(
        self,
        customer: Customer,
        /,
        *,
        receipt_printer: IReceiptPrinter = ReceiptPrinter(),
    ) -> None:
        """Serves the specified customer if any cash registers are open, otherwise the customer will
        need to wait until one is vacant."""
        assert (
            self.__remaining_shifts > 0
        ), "The store is closed and cannot serve any more customers."

        self.__cash_register.open_receipt()
        self.__cash_register.process_items(customer.item_list)
        self.__cash_register.print_receipt(printer=receipt_printer)
        self.__cash_register.accept_payment(customer.payment)

        receipt = self.__cash_register.close_receipt()

        self.__receipt_handler.handle(receipt)

    def has_shifts_remaining(self) -> bool:
        """Returns true if the store has shifts remaining."""
        return self.__remaining_shifts > 0

    def generate_report(self, *, report_printer: IReportPrinter) -> None:
        """Generates a data for the store using the specified data printer."""
        assert (
            self.__remaining_shifts > 0
        ), "The store is closed and cannot generate a report."

        report_printer.print(self.__data_base)

    def generate_shift_report(
        self, *, report_printer: Optional[IReportPrinter] = None
    ) -> None:
        """Generates a report for the current shift and ends it (clears the store data)."""
        assert (
            self.__remaining_shifts > 0
        ), "The store is closed and cannot generate a report."

        if report_printer is not None:
            report_printer.print(self.__data_base)

        self.__data_base.clear_sold_items()
        self.__remaining_shifts -= 1

    def __get_default_handler(self) -> IReceiptHandler:
        """Creates a default handler for this store if one is not specified."""
        return ReceiptDataBaseHandler(data_base=self.__data_base)
