from dataclasses import dataclass
from typing import Dict, Iterable, List

from core import IDiscount, Item


@dataclass(frozen=True)
class ItemEntry:
    item: Item
    price: float


class MemoryDataBase:
    def __init__(
        self, *, items: Iterable[ItemEntry] = (), discounts: Iterable[IDiscount] = ()
    ) -> None:
        """Creates a new MemoryDataBase from the specified initial data. This DB does not perform any integrity
        checks across the items it holds, the discounts or the sold units."""
        self.__items: Dict[Item, ItemEntry] = {}
        self.__discounts: Dict[Item, List[IDiscount]] = {}
        self.__sold_items: Dict[Item, int] = {}
        self.__total_revenue: float = 0.0

        self.__initialize_items(items)
        self.__initialize_discounts(discounts)

    def __initialize_items(self, items: Iterable[ItemEntry]) -> None:
        """Initializes the dictionary storing information on the existing items."""
        for item_entry in items:
            assert (
                item_entry.item not in self.__items
            ), f"The item {item_entry.item} is present in the initial data more than once."

            self.__items[item_entry.item] = item_entry

    def __initialize_discounts(self, discounts: Iterable[IDiscount]) -> None:
        """Initializes the dictionary storing information on the existing discounts."""
        for discount in discounts:
            self.__discounts.setdefault(discount.get_related_item(), [])
            self.__discounts[discount.get_related_item()].append(discount)

    def add_item_entry(self, item_entry: ItemEntry) -> None:
        """Adds the specified item entry to this data base."""
        assert (
            item_entry.item not in self.__items
        ), f"The item {item_entry.item} is already present in the data base."

        self.__items[item_entry.item] = item_entry

    def add_discount(self, discount: IDiscount) -> None:
        """Adds the specified discount to this data base."""
        self.__discounts.setdefault(discount.get_related_item(), [])
        self.__discounts[discount.get_related_item()].append(discount)

    def get_item_price(self, item: Item) -> float:
        """Returns the total_price of the specified item from the data base."""
        assert item in self.__items, f"The item {item} is not present in the data base."

        return self.__items[item].price

    def get_item_discounts(self, item: Item) -> List[IDiscount]:
        """Returns a list of all discounts related to the specified item."""
        return self.__discounts.get(item, []).copy()

    def add_sold_item(
        self, item: Item, /, *, total_price: float, count: int = 1
    ) -> None:
        """Adds the the given item of the specified amount to the data base's sold item data."""
        assert (
            count > 0
        ), f"Item count must be a positive number. Received: {count} for item: {item}."

        self.__sold_items.setdefault(item, 0)
        self.__sold_items[item] += count
        self.__total_revenue += total_price

    def get_sold_item_count(self, item: Item) -> int:
        """Returns the count of units sold for the specified item."""
        assert item in self.__sold_items, f"No units of {item} have been sold."

        return self.__sold_items[item]

    def get_distinct_sold_items(self) -> List[Item]:
        """Returns a list of all distinct items sold."""
        return list(self.__sold_items.keys())

    def get_total_revenue(self) -> float:
        """Returns the total revenue earned from all sold items."""
        return self.__total_revenue

    def clear_sold_items(self) -> None:
        """Clears all data about sold items."""
        self.__sold_items.clear()
        self.__total_revenue = 0.0
