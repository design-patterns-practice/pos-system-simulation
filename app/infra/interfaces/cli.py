from typing import Final

from core import ICustomerFactory, StoreService

REPORT_PROMPT: Final[
    str
] = "Would you like to print a report of the store's state? [Y/n]"

SHIFT_PROMPT: Final[
    str
] = "Would you like to end the store's current shift and clear all data? [Y/n]"


class CLI:
    def __init__(
        self,
        *,
        store: StoreService,
        customer_factory: ICustomerFactory,
        n_customers_per_report_prompt: int,
        n_customers_per_shift_prompt: int,
        receipt_printer: StoreService.IReceiptPrinter,
        report_printer: StoreService.IReportPrinter,
    ) -> None:
        self.__store = store
        self.__customer_factory = customer_factory
        self.__n_customers_per_report_prompt = n_customers_per_report_prompt
        self.__n_customers_per_shift_prompt = n_customers_per_shift_prompt
        self.__receipt_printer = receipt_printer
        self.__report_printer = report_printer

    def simulate(self) -> None:
        """Runs a simple simulation of the store."""

        shift_index = 0
        customer_index = 0

        print("Simulation Started...\n")
        self.__store.open_store(clear_data=True)

        while self.__store.has_shifts_remaining():

            shift_index += 1

            print(
                f"\n# =============== Shift N{shift_index} has started ============= #\n"
            )

            while True:

                customer = self.__customer_factory.next_customer()
                customer_index += 1

                print(f"\nServing customer N{customer_index}.\n")
                self.__store.serve_customer(
                    customer, receipt_printer=self.__receipt_printer
                )

                if customer_index % self.__n_customers_per_report_prompt == 0:
                    answer = input(REPORT_PROMPT)

                    if answer.upper() == "Y":
                        self.__store.generate_report(
                            report_printer=self.__report_printer
                        )

                if customer_index % self.__n_customers_per_shift_prompt == 0:
                    answer = input(SHIFT_PROMPT)

                    if answer.upper() == "Y":
                        self.__store.generate_shift_report()
                        break

            print(
                f"\n# =============== Shift N{shift_index} has ended =============== #\n"
            )

        print("Simulation Complete.\n")
