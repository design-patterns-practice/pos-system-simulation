class CashPayment:
    @staticmethod
    def transfer(amount: float, /) -> None:
        """Executes this payment by transferring the specified amount."""
        print(f"Customer paid {amount:.2f} with cash.")


class CardPayment:
    @staticmethod
    def transfer(amount: float, /) -> None:
        """Executes this payment by transferring the specified amount."""
        print(f"Customer paid {amount:.2f} with card.")
