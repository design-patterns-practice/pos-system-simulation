from typing import Callable, Final, List

from core import (
    CashRegisterFactory,
    Discount,
    IDiscount,
    IPayment,
    Item,
    RandomCustomerFactory,
    ReceiptDataBaseHandler,
    ReceiptPrinter,
    ReportPrinter,
    StoreService,
)
from infra import CLI, CardPayment, CashPayment, ItemEntry, MemoryDataBase

Application = Callable[[], None]

# === Simulation Data ========================================= #

ITEM_ENTRIES: Final[List[ItemEntry]] = [
    ItemEntry(item="Bread", price=1.0),
    ItemEntry(item="Milk", price=2.5),
    ItemEntry(item="Butter", price=2.0),
    ItemEntry(item="Mustard", price=0.3),
    ItemEntry(item="Comic Book", price=1.5),
    ItemEntry(item="Beer", price=1.2),
    ItemEntry(item="Beer Pack", price=7),
    ItemEntry(item="A Bucket Load of Beer", price=50),
    ItemEntry(item="Tissue Pack", price=1.5),
    ItemEntry(item="Washing Machine", price=1500.99),
    ItemEntry(item="Bugs Bunny Costume", price=199.99),
]

ITEMS: Final[List[Item]] = [entry.item for entry in ITEM_ENTRIES]

DISCOUNTS: Final[List[IDiscount]] = [
    Discount(
        related_item="Beer Pack",
        required_items={"A Bucket Load of Beer": 1},
        reduction_percentage=95,
    ),
    Discount(
        related_item="Tissue Pack",
        required_items={"Tissue Pack": 2},
        reduction_percentage=10,
    ),
    Discount(
        related_item="Washing Machine",
        required_items={},
        reduction_percentage=50,
    ),
    Discount(
        related_item="Bugs Bunny Costume",
        required_items={"A Bucket Load of Beer": 1, "Washing Machine": 1},
        reduction_percentage=99,
    ),
]

PAYMENT_METHODS: Final[List[IPayment]] = [CashPayment(), CardPayment()]

CASH_REGISTERS: Final[int] = 4  # Currently doesn't matter.
DAILY_SHIFTS: Final[int] = 3

N_CUSTOMERS_PER_REPORT_PROMPT: Final[int] = 20
N_CUSTOMERS_PER_SHIFT_PROMPT: Final[int] = 100

# ============================================================= #


def setup() -> Application:
    def app() -> None:
        data_base = MemoryDataBase(items=ITEM_ENTRIES, discounts=DISCOUNTS)
        cash_register_factory = CashRegisterFactory()

        # Can be chained with other handlers.
        receipt_handler = ReceiptDataBaseHandler(data_base=data_base)

        store = StoreService(
            cash_registers=CASH_REGISTERS,
            daily_shifts=DAILY_SHIFTS,
            cash_register_factory=cash_register_factory,
            data_base=data_base,
            receipt_handler=receipt_handler,
        )

        CLI(
            store=store,
            customer_factory=RandomCustomerFactory(
                possible_items=ITEMS, payment_methods=PAYMENT_METHODS
            ),
            n_customers_per_report_prompt=N_CUSTOMERS_PER_REPORT_PROMPT,
            n_customers_per_shift_prompt=N_CUSTOMERS_PER_SHIFT_PROMPT,
            receipt_printer=ReceiptPrinter(),
            report_printer=ReportPrinter(),
        ).simulate()

    return app
