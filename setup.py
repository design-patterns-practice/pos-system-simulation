from setuptools import setup

setup(
    name="POS System Simulation",
    version="1.1.0",
    description="A simple POS System simulation with a CLI.",
    package_dir={"": "app"},
    author="Zurab Mujirishvili",
    author_email="zmuji18@freeuni.edu.ge",
)
