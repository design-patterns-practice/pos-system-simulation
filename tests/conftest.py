import pytest
from core import PriceCalculator
from infra import MemoryDataBase


@pytest.fixture
def memory_data_base() -> MemoryDataBase:
    return MemoryDataBase()


@pytest.fixture
def price_calculator(memory_data_base: MemoryDataBase) -> PriceCalculator:
    return PriceCalculator(data_base=memory_data_base)
