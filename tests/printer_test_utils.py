from typing import Any, List, Tuple
from unittest.mock import Mock


def get_mocks(*, formatter_output: str = "") -> Tuple[Mock, Mock]:
    """Returns a mock for an output stream and a formatter. The mock formatter output can be specified."""
    output_stream = Mock()
    formatter = Mock()
    formatter.format = Mock(return_value=formatter_output)

    return output_stream, formatter


def check_result(
    *,
    formatter: Mock,
    output_stream: Mock,
    expected_header: List[str],
    expected_rows: List[List[Any]],
    expected_summary: str,
    expected_stream_input: str,
) -> None:

    formatter.format.assert_called_once()
    formatter_input = formatter.format.call_args.kwargs

    output_stream.assert_called_once()
    output_stream_input = output_stream.call_args.args[0]

    assert formatter_input["header"] == expected_header
    assert formatter_input["rows"] == expected_rows
    assert formatter_input["summary"] == expected_summary
    assert output_stream_input == expected_stream_input
