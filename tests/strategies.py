from typing import Tuple

from core import (
    CashRegisterFactory,
    Discount,
    ICashRegisterFactory,
    IDataBase,
    IDiscount,
    IPayment,
    IPriceCalculator,
    Item,
    PriceCalculator,
    StoreService,
)
from hypothesis.strategies import (
    SearchStrategy,
    builds,
    dictionaries,
    floats,
    integers,
    lists,
    one_of,
    sampled_from,
    text,
)
from infra import CardPayment, CashPayment, ItemEntry, MemoryDataBase


def positive_integers() -> SearchStrategy[int]:
    """Returns a positive integer."""
    return integers(min_value=1)


def negative_integers() -> SearchStrategy[int]:
    """Returns a negative integer."""
    return integers(max_value=-1)


def cash_register_factories() -> SearchStrategy[ICashRegisterFactory]:
    """Returns a cash register factory."""
    return sampled_from([CashRegisterFactory()])


def memory_data_bases() -> SearchStrategy[MemoryDataBase]:
    """Returns an in-memory data base."""

    def get_item(entry: ItemEntry) -> str:
        return entry.item

    return builds(
        lambda item_list, discount_list: MemoryDataBase(
            items=item_list, discounts=discount_list
        ),
        item_list=lists(item_entries(), unique_by=get_item, max_size=5),
        discount_list=lists(discounts(), max_size=3),
    )


def data_bases() -> SearchStrategy[IDataBase]:
    """Returns a data base implementation."""
    return one_of(memory_data_bases())


def stores() -> SearchStrategy[StoreService]:
    """Returns a configured store."""
    return builds(
        lambda cash_registers, daily_shifts, cash_register_factory, data_base: StoreService(
            cash_registers=cash_registers,
            daily_shifts=daily_shifts,
            cash_register_factory=cash_register_factory,
            data_base=data_base,
        ),
        cash_registers=positive_integers(),
        daily_shifts=positive_integers(),
        cash_register_factory=cash_register_factories(),
        data_base=data_bases(),
    )


def items() -> SearchStrategy[Item]:
    """Returns an item."""
    return text(min_size=1)


def priced_items() -> SearchStrategy[Tuple[Item, float]]:
    """Returns a priced item"""
    return builds(
        lambda item, price: (item, price),
        item=items(),
        price=floats(min_value=0, exclude_min=True),
    )


def item_entries() -> SearchStrategy[ItemEntry]:
    """Returns an item entry for an in memory data base."""
    return builds(
        lambda item, price: ItemEntry(item=item, price=price),
        item=items(),
        price=floats(min_value=0, exclude_min=True),
    )


def discounts() -> SearchStrategy[IDiscount]:
    """Returns a discount for an in memory data base."""
    return builds(
        lambda related_item, required_items, reduction_percentage: Discount(
            related_item=related_item,
            required_items=required_items,
            reduction_percentage=reduction_percentage,
        ),
        related_item=items(),
        required_items=dictionaries(
            keys=items(), values=positive_integers(), max_size=4
        ),
        reduction_percentage=floats(min_value=0, exclude_min=True),
    )


def price_calculators() -> SearchStrategy[IPriceCalculator]:
    return builds(
        lambda data_base: PriceCalculator(data_base=data_base), data_base=data_bases()
    )


def payments() -> SearchStrategy[IPayment]:
    return sampled_from([CashPayment(), CardPayment()])
