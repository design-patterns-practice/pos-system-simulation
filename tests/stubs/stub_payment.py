from dataclasses import dataclass, field


@dataclass
class StubPayment:
    transfer_amount: float = field(init=False)

    def transfer(self, amount: float, /) -> None:
        """Executes this payment by transferring the specified amount."""
        self.transfer_amount = amount
