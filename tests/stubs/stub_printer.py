from dataclasses import dataclass, field
from typing import Dict, List

import pytest
from core import IReceipt, IReport, Item


@dataclass
class StubReceiptPrinter:
    receipt: IReceipt = field(init=False)

    def print(self, receipt: IReceipt, /) -> None:
        """Prints the specified receipt."""
        self.receipt = receipt


@dataclass
class StubReportPrinter:
    expected_item_counts: Dict[Item, int] = field(init=False)
    expected_distinct_items: List[Item] = field(init=False)
    expected_revenue: float = field(init=False)

    def print(self, report: IReport, /) -> None:
        """Validates the received report against the specified expected results."""
        for item, count in self.expected_item_counts.items():
            assert report.get_sold_item_count(item) == count

        assert sorted(report.get_distinct_sold_items()) == sorted(
            self.expected_distinct_items
        )
        assert report.get_total_revenue() == pytest.approx(self.expected_revenue)

    def expect(
        self,
        *,
        item_counts: Dict[Item, int],
        distinct_items: List[Item],
        revenue: float,
    ) -> None:
        self.expected_item_counts = item_counts
        self.expected_distinct_items = distinct_items
        self.expected_revenue = revenue
