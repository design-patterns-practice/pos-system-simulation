"""
Test List for Cash Register:
1) Should create cash register. V
2) Should open receipt for new customer. V
2) Should process customer items (applying any available discounts). V
4) Should print the receipt to the specified printer. V
5) Should accept payment with the specified payment method. V
6) Should close and return receipt. V
"""

from core import (
    CashRegister,
    Discount,
    IPriceCalculator,
    PriceCalculator,
    ReceiptPrinter,
)
from infra import CashPayment, ItemEntry, MemoryDataBase
from stubs import StubPayment, StubReceiptPrinter


def test_should_create_cash_register(price_calculator: IPriceCalculator) -> None:
    CashRegister(price_calculator=price_calculator)


def test_should_open_new_receipt_for_customer(
    price_calculator: IPriceCalculator,
) -> None:
    cash_register = CashRegister(price_calculator=price_calculator)
    cash_register.open_receipt()


def test_should_process_customer_items() -> None:
    item_list = [
        "Item 1",
        "Item 2",
        "Item 3",
        "Item 2",
    ]

    data_base = MemoryDataBase(
        items=[ItemEntry(item=item, price=1.0) for item in set(item_list)]
    )

    cash_register = CashRegister(price_calculator=PriceCalculator(data_base=data_base))

    cash_register.open_receipt()
    cash_register.process_items(item_list)


def test_should_pass_receipt_to_printer() -> None:
    data_base = MemoryDataBase(
        items=[
            ItemEntry(item="Item 1", price=4.0),
            ItemEntry(item="Item 2", price=2.3),
            ItemEntry(item="Item 3", price=5.9),
        ],
        discounts=[
            Discount(
                related_item="Item 1",
                required_items={"Item 1": 2, "Item 2": 1},
                reduction_percentage=10,
            ),
            Discount(
                related_item="Item 5",
                required_items={"Item 1": 2, "Item 2": 5},
                reduction_percentage=50,
            ),
            Discount(
                related_item="Item 2",
                required_items={"Item 2": 1},
                reduction_percentage=15,
            ),
        ],
    )

    item_list = [
        "Item 1",
        "Item 1",
        "Item 2",
        "Item 3",
    ]

    printer = StubReceiptPrinter()
    price_calculator = PriceCalculator(data_base=data_base)
    cash_register = CashRegister(price_calculator=price_calculator)

    cash_register.open_receipt()
    cash_register.process_items(item_list)
    cash_register.print_receipt(printer=printer)

    expected_item_list = ["Item 1", "Item 2", "Item 3"]
    expected_cost = (
        (2 * 4.0 * (100 - 10) / 100) + (1 * 2.3 * (100 - 15) / 100) + (1 * 5.9)
    )

    assert printer.receipt is not None
    assert sorted(printer.receipt.get_distinct_items()) == expected_item_list
    assert printer.receipt.get_total_cost() == expected_cost


def test_should_receive_payment() -> None:
    data_base = MemoryDataBase(
        items=[
            ItemEntry(item="Item 1", price=1.2),
            ItemEntry(item="Item 2", price=2.3),
            ItemEntry(item="Item 3", price=5.6),
        ]
    )

    cash_register = CashRegister(price_calculator=PriceCalculator(data_base=data_base))

    cash_register.open_receipt()
    cash_register.process_items(["Item 1", "Item 3"])
    cash_register.print_receipt(printer=ReceiptPrinter())

    payment = StubPayment()
    cash_register.accept_payment(payment)

    assert payment.transfer_amount == 6.8


def test_should_close_and_return_correct_receipt() -> None:
    data_base = MemoryDataBase(
        items=[
            ItemEntry(item="Item 1", price=1.2),
            ItemEntry(item="Item 2", price=5.3),
        ]
    )

    cash_register = CashRegister(price_calculator=PriceCalculator(data_base=data_base))

    cash_register.open_receipt()
    cash_register.process_items(["Item 2", "Item 2"])
    cash_register.print_receipt(printer=ReceiptPrinter())
    cash_register.accept_payment(CashPayment())

    receipt = cash_register.close_receipt()

    assert receipt.get_distinct_items() == ("Item 2",)
    assert receipt.get_item_count("Item 2") == 2
    assert receipt.get_total_cost() == 10.6
