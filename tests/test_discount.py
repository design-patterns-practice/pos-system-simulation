"""
Test List for Discount:
1) Should create discount for item from required items and specified total_price reduction. V
2) Should return correct related item. V
3) Should apply to product with valid context items. V
4) Should not apply to product with invalid context items. V
"""
from typing import Dict

from core import Discount, Item
from hypothesis import assume, given
from hypothesis.strategies import dictionaries, floats
from strategies import items, positive_integers


def test_should_create_discount() -> None:
    Discount(
        related_item="Item 1",
        required_items={
            "Item 2": 2,
            "Item 1": 3,
        },
        reduction_percentage=15,
    )


@given(
    item=items(),
    required_items=dictionaries(keys=items(), values=positive_integers()),
    reduction_percentage=floats(min_value=0, exclude_min=True),
)
def test_should_return_correct_related_item_given(
    item: Item, required_items: Dict[Item, int], reduction_percentage: float
) -> None:
    discount = Discount(
        related_item=item,
        required_items=required_items,
        reduction_percentage=reduction_percentage,
    )

    assert discount.get_related_item() == item


@given(item=items(), reduction_percentage=floats(min_value=0, exclude_min=True))
def test_should_apply_to_related_item_given(
    item: Item, reduction_percentage: float
) -> None:

    other_items = {
        "Item 2": 3,
        "Item 3": 1,
        "Item 1": 2,
    }

    discount = Discount(
        related_item=item,
        required_items={
            "Item 1": 1,
            "Item 2": 1,
        },
        reduction_percentage=reduction_percentage,
    )

    assert discount.applied_to(item, context_items=other_items) == reduction_percentage


@given(item=items(), reduction_percentage=floats(min_value=0, exclude_min=True))
def test_should_not_apply_to_unrelated_item_given(
    item: Item, reduction_percentage: float
) -> None:

    other_items = {
        "Item 2": 3,
        "Item 3": 1,
        "Item 1": 2,
    }

    discount = Discount(
        related_item="Item 3",
        required_items={
            "Item 1": 1,
            "Item 2": 1,
        },
        reduction_percentage=reduction_percentage,
    )

    assume(item != "Item 3")

    assert discount.applied_to(item, context_items=other_items) == 0


@given(item=items(), reduction_percentage=floats(min_value=0, exclude_min=True))
def test_should_not_apply_to_item_given(
    item: Item, reduction_percentage: float
) -> None:

    other_items = {
        "Item 2": 3,
        "Item 3": 1,
        "Item 1": 2,
    }

    discount = Discount(
        related_item=item,
        required_items={
            "Item 1": 3,
            "Item 2": 2,
            "Item 3": 1,
        },
        reduction_percentage=reduction_percentage,
    )

    assert discount.applied_to(item, context_items=other_items) == 0
