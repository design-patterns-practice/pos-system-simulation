"""
Test List for Memory Data Base:
1) Should create blank data base. V
2) Should create data base from initial data. V
3) Should add new item to data base. V
4) Should add new discount to data base. V
5) Should return total_price for specified item in data base. V
6) Should return related discount list for specified item. V
7) Should add sold items to the data base. V
8) Should return the count of sold units for items. V
9) Should return a list of distinct items sold. V
10) Should return the total revenue from all sold items. V
11) Should clear sold item data. V
"""
from random import choice, shuffle
from typing import List, Tuple

from core import Discount, IDiscount, Item
from hypothesis import assume, given
from hypothesis.strategies import lists, tuples
from infra import ItemEntry, MemoryDataBase
from strategies import discounts, item_entries, items, positive_integers


def get_item(entry: ItemEntry) -> str:
    return entry.item


def test_should_create_blank_data_base() -> None:
    MemoryDataBase()


def test_should_create_data_base_from_initial_data() -> None:
    MemoryDataBase(
        items=[
            ItemEntry(item="Item 1", price=10.5),
            ItemEntry(item="Item 2", price=1.7),
            ItemEntry(item="Item 3", price=0.9),
            ItemEntry(item="Item 4", price=100.5),
        ],
        discounts=[
            Discount(
                related_item="Item 2",
                required_items={
                    "Item 2": 2,
                },
                reduction_percentage=5,
            ),
            Discount(
                related_item="Item 4",
                required_items={
                    "Item 2": 1,
                    "Item 4": 1,
                },
                reduction_percentage=5,
            ),
        ],
    )


@given(
    item_list=lists(item_entries(), unique_by=get_item),
    discount_list=lists(discounts()),
)
def test_should_create_data_base_from_initial_data_given(
    item_list: List[ItemEntry], discount_list: List[IDiscount]
) -> None:
    MemoryDataBase(items=item_list, discounts=discount_list)


def test_should_add_new_items_to_data_base() -> None:
    data_base = MemoryDataBase()

    data_base.add_item_entry(ItemEntry(item="Item 1", price=10.5))
    data_base.add_item_entry(ItemEntry(item="Item 2", price=11.5))
    data_base.add_item_entry(ItemEntry(item="Item 3", price=15.2))


def test_should_add_new_discount_to_data_base() -> None:
    data_base = MemoryDataBase()

    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 1": 2},
            reduction_percentage=5,
        )
    )
    data_base.add_discount(
        Discount(
            related_item="Item 2",
            required_items={"Item 1": 3},
            reduction_percentage=11.5,
        )
    )
    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 1": 1, "Item 2": 1},
            reduction_percentage=15.2,
        )
    )


@given(discount_list=lists(discounts()))
def test_should_add_new_discount_to_data_base_given(
    discount_list: List[IDiscount],
) -> None:
    discount_count_half = len(discount_list) // 2
    data_base = MemoryDataBase(discounts=discount_list[:discount_count_half])

    for discount in discount_list[discount_count_half:]:
        data_base.add_discount(discount)


def test_should_return_price_for_item() -> None:
    data_base = MemoryDataBase(
        items=[
            ItemEntry(item="Item 1", price=10.5),
            ItemEntry(item="Item 2", price=1.7),
            ItemEntry(item="Item 3", price=0.9),
            ItemEntry(item="Item 4", price=100.5),
        ]
    )

    assert data_base.get_item_price("Item 1") == 10.5
    assert data_base.get_item_price("Item 2") == 1.7
    assert data_base.get_item_price("Item 4") == 100.5
    assert data_base.get_item_price("Item 3") == 0.9


@given(item_list=lists(item_entries(), unique_by=get_item))
def test_should_return_price_for_item_given(item_list: List[ItemEntry]) -> None:
    data_base = MemoryDataBase()

    for item_entry in item_list:
        data_base.add_item_entry(item_entry)

    shuffle(item_list)

    for item_entry in item_list:
        assert data_base.get_item_price(item_entry.item) == item_entry.price


def test_should_return_related_discounts() -> None:
    discount_1 = Discount(
        related_item="Item 1",
        required_items={"Item 1": 2},
        reduction_percentage=5,
    )
    discount_2 = Discount(
        related_item="Item 2",
        required_items={"Item 1": 3},
        reduction_percentage=11.5,
    )
    discount_3 = Discount(
        related_item="Item 1",
        required_items={"Item 1": 1, "Item 2": 1},
        reduction_percentage=15.2,
    )

    data_base = MemoryDataBase(discounts=[discount_1, discount_2])

    assert data_base.get_item_discounts("Item 1") == [discount_1]
    assert data_base.get_item_discounts("Item 2") == [discount_2]

    data_base.add_discount(discount_3)

    assert data_base.get_item_discounts("Item 1") == [discount_1, discount_3]


@given(discount_list=lists(discounts()))
def test_should_return_related_discounts_given(
    discount_list: List[IDiscount],
) -> None:
    discount_count_half = len(discount_list) // 2
    first_half = discount_list[:discount_count_half]
    second_half = discount_list[discount_count_half:]

    data_base = MemoryDataBase(discounts=first_half)

    shuffle(first_half)

    for discount in first_half:
        assert discount in data_base.get_item_discounts(discount.get_related_item())

    for discount in second_half:
        data_base.add_discount(discount)

    shuffle(second_half)

    for discount in second_half:
        assert discount in data_base.get_item_discounts(discount.get_related_item())


def test_should_return_empty_discount_list() -> None:

    data_base = MemoryDataBase(
        discounts=[
            Discount(
                related_item="Item 2",
                required_items={"Item 1": 3},
                reduction_percentage=11.5,
            ),
            Discount(
                related_item="Item 3",
                required_items={"Item 1": 1, "Item 2": 1},
                reduction_percentage=15.2,
            ),
        ]
    )

    assert data_base.get_item_discounts("Item 1") == []


@given(discount_list=lists(discounts()))
def test_should_return_empty_discount_list_given(
    discount_list: List[IDiscount],
) -> None:

    assume("Item 1" not in {discount.get_related_item() for discount in discount_list})

    data_base = MemoryDataBase(discounts=discount_list)

    assert data_base.get_item_discounts("Item 1") == []


def test_should_add_sold_items() -> None:
    data_base = MemoryDataBase()

    data_base.add_sold_item("Item 1", total_price=1.0, count=10)
    data_base.add_sold_item("Item 3", total_price=3.0, count=2)
    data_base.add_sold_item("Item 5", total_price=2.0, count=200)


@given(item_counts=lists(tuples(items(), positive_integers())))
def test_should_add_sold_items_given(item_counts: List[Tuple[Item, int]]) -> None:
    data_base = MemoryDataBase()

    for item, count in item_counts:
        data_base.add_sold_item(item, total_price=3.0, count=count)


def test_should_return_sold_item_count() -> None:
    data_base = MemoryDataBase()

    data_base.add_sold_item("Item 1", total_price=1.0, count=10)
    data_base.add_sold_item("Item 2", total_price=1.0, count=1)
    data_base.add_sold_item("Item 3", total_price=1.0, count=200)

    assert data_base.get_sold_item_count("Item 2") == 1
    assert data_base.get_sold_item_count("Item 3") == 200
    assert data_base.get_sold_item_count("Item 1") == 10

    data_base.add_sold_item("Item 2", total_price=2.0, count=4)

    assert data_base.get_sold_item_count("Item 2") == 5


@given(item_counts=lists(tuples(items(), positive_integers())))
def test_should_return_sold_item_count_given(
    item_counts: List[Tuple[Item, int]]
) -> None:
    data_base = MemoryDataBase()

    for item, count in item_counts:
        data_base.add_sold_item(item, total_price=5.0, count=count)

    shuffle(item_counts)

    for item, count in item_counts:
        assert data_base.get_sold_item_count(item) >= count

    # Add the same items again.
    for item, count in item_counts:
        data_base.add_sold_item(item, total_price=1.0, count=count)

    shuffle(item_counts)

    # They should now be at least doubled.
    for item, count in item_counts:
        assert data_base.get_sold_item_count(item) >= 2 * count


def test_should_return_distinct_sold_items() -> None:
    data_base = MemoryDataBase()

    data_base.add_sold_item("Item 1", total_price=5.0, count=10)
    data_base.add_sold_item("Item 2", total_price=5.0, count=1)

    assert sorted(data_base.get_distinct_sold_items()) == ["Item 1", "Item 2"]

    data_base.add_sold_item("Item 3", total_price=1.0, count=200)
    data_base.add_sold_item("Item 2", total_price=1.0, count=30)
    data_base.add_sold_item("Item 1", total_price=1.0, count=2)

    assert sorted(data_base.get_distinct_sold_items()) == ["Item 1", "Item 2", "Item 3"]


@given(item_list=lists(items()), counts=lists(positive_integers(), min_size=1))
def test_should_return_distinct_sold_items_given(
    item_list: List[Item], counts: List[int]
) -> None:
    data_base = MemoryDataBase()

    for item in item_list:
        data_base.add_sold_item(item, total_price=1.0, count=choice(counts))

    assert sorted(data_base.get_distinct_sold_items()) == sorted(set(item_list))


def test_should_return_total_revenue() -> None:
    data_base = MemoryDataBase()

    assert data_base.get_total_revenue() == 0.0

    data_base.add_sold_item("Item 1", total_price=25.0)
    data_base.add_sold_item("Item 1", total_price=15.0)
    data_base.add_sold_item("Item 2", total_price=5.0)

    assert data_base.get_total_revenue() == 45.0


def test_should_clear_sold_item_data() -> None:
    data_base = MemoryDataBase()

    data_base.add_sold_item("Item 2", total_price=2.5, count=10)
    data_base.add_sold_item("Item 5", total_price=22.5, count=10)
    data_base.add_sold_item("Doesn't have to start with item", total_price=5, count=10)

    data_base.clear_sold_items()

    assert data_base.get_distinct_sold_items() == []
    assert data_base.get_total_revenue() == 0.0


@given(item_list=lists(items()), counts=lists(positive_integers(), min_size=1))
def test_should_clear_sold_item_data_given(
    item_list: List[Item], counts: List[int]
) -> None:
    data_base = MemoryDataBase()

    for item in item_list:
        data_base.add_sold_item(item, total_price=4.5, count=choice(counts))

    data_base.clear_sold_items()

    assert data_base.get_distinct_sold_items() == []
    assert data_base.get_total_revenue() == 0.0
