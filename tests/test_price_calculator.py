"""
Test List for Price Calculator:
1) Should create total_price calculator from specified database. V
2) Should calculate total_price for item that doesn't have a discount. V
3) Should calculate total_price for item with discounts. V
"""
from core import Discount, IDataBase, PriceCalculator
from infra import ItemEntry, MemoryDataBase


def test_should_create_price_calculator_from_data_base_given(
    memory_data_base: IDataBase,
) -> None:
    PriceCalculator(data_base=memory_data_base)


def test_should_calculate_price_for_item_without_discount() -> None:
    data_base = MemoryDataBase()
    data_base.add_item_entry(ItemEntry(item="Item 1", price=5.0))
    data_base.add_item_entry(ItemEntry(item="Item 2", price=3.3))
    data_base.add_item_entry(ItemEntry(item="Item 3", price=0.2))

    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 3": 5},
            reduction_percentage=20.0,
        )
    )

    price_calculator = PriceCalculator(data_base=data_base)

    assert price_calculator.price_of("Item 1", context_items={"Item 1": 1}) == 5.0
    assert price_calculator.price_of("Item 2", context_items={"Item 2": 1}) == 3.3
    assert price_calculator.price_of("Item 3", context_items={"Item 3": 4}) == 0.2


def test_should_calculate_price_for_item_with_discounts() -> None:
    data_base = MemoryDataBase()
    data_base.add_item_entry(ItemEntry(item="Item 1", price=5.0))
    data_base.add_item_entry(ItemEntry(item="Item 2", price=3.3))
    data_base.add_item_entry(ItemEntry(item="Item 3", price=0.2))

    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 1": 1},
            reduction_percentage=20.0,
        )
    )
    data_base.add_discount(
        Discount(
            related_item="Item 2",
            required_items={"Item 2": 2},
            reduction_percentage=10.0,
        )
    )

    price_calculator = PriceCalculator(data_base=data_base)

    assert (
        price_calculator.price_of("Item 1", context_items={"Item 1": 3})
        == 5.0 * (100 - 20) / 100
    )
    assert (
        price_calculator.price_of("Item 2", context_items={"Item 1": 2, "Item 2": 3})
        == 3.3 * (100 - 10) / 100
    )
    assert price_calculator.price_of("Item 3", context_items={"Item 1": 10}) == 0.2

    # Add multiple discounts.
    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 1": 2},
            reduction_percentage=30.0,
        )
    )

    assert (
        price_calculator.price_of("Item 1", context_items={"Item 1": 2, "Item 2": 4})
        == 5.0 * (100 - 20 - 30) / 100  # Now two discounts apply.
    )
    assert (
        price_calculator.price_of("Item 2", context_items={"Item 1": 2, "Item 2": 3})
        == 3.3 * (100 - 10) / 100  # The same discount applies for the remaining items.
    )
    assert price_calculator.price_of("Item 3", context_items={"Item 1": 10}) == 0.2


def test_should_return_zero_for_over_discounted_item() -> None:
    data_base = MemoryDataBase()
    data_base.add_item_entry(ItemEntry(item="Item 1", price=5.0))
    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 1": 1},
            reduction_percentage=20.0,
        )
    )
    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 2": 2},
            reduction_percentage=10.0,
        )
    )
    data_base.add_discount(
        Discount(
            related_item="Item 1",
            required_items={"Item 3": 2},
            reduction_percentage=80.0,
        )
    )

    price_calculator = PriceCalculator(data_base=data_base)

    assert (
        price_calculator.price_of(
            "Item 1",
            context_items={
                "Item 1": 3,
                "Item 2": 2,
                "Item 3": 5,
            },
        )
        == 0
    )
