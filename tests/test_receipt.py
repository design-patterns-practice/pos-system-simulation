"""
Test List for Receipt:
1) Should create an empty receipt containing no items. V
2) Should add items to the receipt one by one. V
3) Should return a tuple of distinct items added to the receipt. V
4) Should return the count of a given item added to the receipt. V
5) Should return the average total_price of a given item added to the receipt. V
5) Should return the total total_price of a given item added to the receipt. V
4) Should return the total cost of all items in the receipt. V
"""
from typing import List, Tuple

from core import IReceipt, Item, Receipt
from hypothesis import example, given
from hypothesis.strategies import lists
from strategies import priced_items

PricedItem = Tuple[Item, float]


def add_to_receipt(
    receipt: IReceipt, *, item: Item, price: float = 1.0, times: int = 1
) -> None:
    """Adds the specified item to the given receipt multiple times."""
    for _ in range(times):
        receipt.add_item(item, price=price)


def test_should_create_blank_receipt() -> None:
    receipt = Receipt()

    assert receipt is not None
    assert len(receipt.get_distinct_items()) == 0
    assert receipt.get_total_cost() == 0


def test_should_add_items_to_receipt() -> None:
    receipt = Receipt()
    receipt.add_item("Random Item", price=2.3)
    receipt.add_item("Another Item", price=5.4)
    receipt.add_item("Last One", price=1.0)


@given(item_list=lists(priced_items()))
@example(
    item_list=[
        ("Item 1", 5.5),
        ("Item 2", 2.3),
        ("Item 3", 21.2),
    ]
)
def test_should_add_items_to_receipt_given(item_list: List[PricedItem]) -> None:
    receipt = Receipt()
    for item, price in item_list:
        receipt.add_item(item, price=price)


def test_should_return_distinct_items_added_to_receipt() -> None:
    receipt = Receipt()
    accumulated_item_list: List[Item] = []

    item_1 = "Random Item"
    item_2 = "Another Item"
    item_3 = "Last One"

    receipt.add_item(item_1, price=1.2)
    accumulated_item_list.append(item_1)
    assert sorted(receipt.get_distinct_items()) == sorted(accumulated_item_list)

    receipt.add_item(item_2, price=2.55)
    accumulated_item_list.append(item_2)
    assert sorted(receipt.get_distinct_items()) == sorted(accumulated_item_list)

    receipt.add_item(item_3, price=20.3)
    accumulated_item_list.append(item_3)
    assert sorted(receipt.get_distinct_items()) == sorted(accumulated_item_list)


def test_should_not_return_duplicate_items() -> None:
    item_1 = "Random Item"
    item_2 = "Another Item"

    expected_item_list: List[Item] = [item_1, item_2]

    receipt = Receipt()
    receipt.add_item(item_1, price=1.2)
    receipt.add_item(item_2, price=3.2)
    receipt.add_item(item_2, price=3.2)
    receipt.add_item(item_1, price=1.1)

    assert sorted(receipt.get_distinct_items()) == sorted(expected_item_list)


def test_should_return_count_of_same_items_in_the_receipt() -> None:
    item_1 = "Item 1"
    item_2 = "Item 2"
    item_3 = "Item 3"

    receipt = Receipt()

    add_to_receipt(receipt, item=item_1, price=1.0, times=5)
    add_to_receipt(receipt, item=item_2, price=2.0, times=3)
    add_to_receipt(receipt, item=item_3, price=1.5, times=1)

    assert receipt.get_item_count(item_1) == 5
    assert receipt.get_item_count(item_2) == 3
    assert receipt.get_item_count(item_3) == 1


def test_should_return_average_cost_of_item_in_the_receipt() -> None:
    item_1 = "Item 1"
    item_2 = "Item 2"

    receipt = Receipt()

    add_to_receipt(receipt, item=item_1, price=3, times=5)
    add_to_receipt(receipt, item=item_1, price=1.5, times=5)
    add_to_receipt(receipt, item=item_2, price=2.5, times=4)

    assert receipt.get_item_cost(item_1) == 2.25
    assert receipt.get_item_cost(item_2) == 2.5


def test_should_return_total_cost_of_same_items_in_the_receipt() -> None:
    item_1 = "Item 1"
    item_2 = "Item 2"

    receipt = Receipt()

    receipt.add_item(item_1, price=2.5)
    receipt.add_item(item_1, price=3.5)
    assert receipt.get_item_total_cost(item_1) == 6.0

    receipt.add_item(item_2, price=5.5)
    assert receipt.get_item_total_cost(item_1) == 6.0

    assert receipt.get_item_total_cost(item_2) == 5.5


def test_should_return_total_cost_of_items() -> None:

    item_list = [
        ("Item 1", 10.2),
        ("Item 2", 2.3),
        ("Item 3", 2.5),
        ("Item 3", 2.4),
    ]

    receipt = Receipt()

    for item, price in item_list:
        receipt.add_item(item, price=price)

    assert receipt.get_total_cost() == (10.2 + 2.3 + 2.5 + 2.4)
