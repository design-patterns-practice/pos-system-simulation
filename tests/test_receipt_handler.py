"""
Test List for Receipt Data Base Handler:
1) Should add receipt data to data base when handling. V
2) Should chain with next handler. V
2) Should pass receipt to next handler. V
"""
from unittest.mock import Mock

from core import IDataBase, Receipt, ReceiptDataBaseHandler
from hypothesis import given
from infra import MemoryDataBase
from strategies import data_bases


@given(data_base=data_bases())
def test_should_create_receipt_data_base_handler_given(data_base: IDataBase) -> None:
    ReceiptDataBaseHandler(data_base=data_base)


def test_should_add_receipt_data_to_data_base() -> None:
    receipt = Receipt()
    receipt.add_item("Item 1", price=2.0)
    receipt.add_item("Item 1", price=2.0)
    receipt.add_item("Item 2", price=4.5)

    data_base = MemoryDataBase()

    ReceiptDataBaseHandler(data_base=data_base).handle(receipt)

    assert sorted(data_base.get_distinct_sold_items()) == ["Item 1", "Item 2"]
    assert data_base.get_sold_item_count("Item 1") == 2
    assert data_base.get_sold_item_count("Item 2") == 1
    assert data_base.get_total_revenue() == 8.5


def test_should_chain_handler_with_next() -> None:
    handler_1 = ReceiptDataBaseHandler(data_base=MemoryDataBase())
    handler_2 = Mock(name="Handler Mock 1")
    handler_3 = Mock(name="Handler Mock 2")

    assert handler_1.chain(handler_2).chain(handler_3) == handler_1
    handler_2.chain.assert_called_once_with(handler_3)


def test_should_pass_receipt_to_next_handler() -> None:
    handler_1 = ReceiptDataBaseHandler(data_base=MemoryDataBase())
    handler_2 = Mock(name="Handler Mock")

    handler_1.chain(handler_2)

    receipt = Receipt()
    handler_1.handle(receipt)

    handler_2.handle.assert_called_once_with(receipt)
