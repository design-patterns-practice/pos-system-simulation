"""
Test List for Receipt Printer:
1) Should print items with single units. V
2) Should print items with multiple units. V
"""
from core import IReceipt, Receipt, ReceiptPrinter
from printer_test_utils import check_result, get_mocks


def test_should_correctly_print_single_unit_items() -> None:
    receipt: IReceipt = Receipt()
    receipt.add_item("Item 1", price=10.5)
    receipt.add_item("Item 2", price=5.3)
    receipt.add_item("Item 3", price=2.5)

    output_stream, formatter = get_mocks(formatter_output="Called and Passed")

    ReceiptPrinter(
        output_stream=output_stream,
        formatter=formatter,
        order_key=lambda row: row[ReceiptPrinter.NAME_COLUMN],
    ).print(receipt)

    check_result(
        formatter=formatter,
        output_stream=output_stream,
        expected_header=["Name", "Units", "Price", "Total"],
        expected_rows=[
            ["Item 1", 1, 10.5, 10.5],
            ["Item 2", 1, 5.3, 5.3],
            ["Item 3", 1, 2.5, 2.5],
        ],
        expected_summary=f"Sum: {18.3:.2f}",
        expected_stream_input="Called and Passed",
    )


def test_should_correctly_print_multiple_unit_items() -> None:
    receipt: IReceipt = Receipt()
    receipt.add_item("Item 1", price=2.5)
    receipt.add_item("Item 1", price=2.5)
    receipt.add_item("Item 1", price=2.5)
    receipt.add_item("Item 2", price=3.3)
    receipt.add_item("Item 2", price=3.3)
    receipt.add_item("Item 3", price=4.5)
    receipt.add_item("Item 3", price=4.5)

    output_stream, formatter = get_mocks(formatter_output="Again Called and Passed")

    ReceiptPrinter(
        output_stream=output_stream,
        formatter=formatter,
        order_key=lambda row: row[ReceiptPrinter.TOTAL_COLUMN],
    ).print(receipt)

    check_result(
        formatter=formatter,
        output_stream=output_stream,
        expected_header=["Name", "Units", "Price", "Total"],
        expected_rows=[
            ["Item 2", 2, 3.3, 6.6],
            ["Item 1", 3, 2.5, 7.5],
            ["Item 3", 2, 4.5, 9.0],
        ],
        expected_summary=f"Sum: {23.1:.2f}",
        expected_stream_input="Again Called and Passed",
    )
