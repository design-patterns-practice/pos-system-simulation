"""
Test List for Report Printer:
1) Should generate data for sold items. V
"""
from typing import List

from core import Item, ReportPrinter
from printer_test_utils import check_result, get_mocks


class MockReport:
    @staticmethod
    def get_sold_item_count(item: Item) -> int:
        """Returns the count of units sold for the specified item."""
        return {
            "Item 1": 4,
            "Item 2": 3,
            "Item 3": 10,
        }[item]

    @staticmethod
    def get_distinct_sold_items() -> List[Item]:
        """Returns a list of all distinct items sold."""
        return ["Item 1", "Item 2", "Item 3"]

    @staticmethod
    def get_total_revenue() -> float:
        """Returns the total revenue earned from all sold items."""
        return 244.5


def test_should_create_report_for_sold_items() -> None:
    report = MockReport()
    output_stream, formatter = get_mocks(formatter_output="Expected Stream Input")

    ReportPrinter(
        output_stream=output_stream,
        formatter=formatter,
        order_key=lambda row: row[ReportPrinter.NAME_COLUMN],
    ).print(report)

    check_result(
        formatter=formatter,
        output_stream=output_stream,
        expected_header=["Name", "Sold"],
        expected_rows=[
            ["Item 1", 4],
            ["Item 2", 3],
            ["Item 3", 10],
        ],
        expected_summary=f"Total Revenue: {244.5:.2f}",
        expected_stream_input="Expected Stream Input",
    )
