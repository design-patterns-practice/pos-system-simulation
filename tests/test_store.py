"""
Test List for Store:
1) Should create a store with some valid configuration. V
2) Should open the store and be able to serve customers. V
3) Should serve a customer with a collection of items. V
4) Should create a report of the current state of the store using report data generator. V
5) Should create a report of the current shift and clear all data. V
6) Should return the number of shifts left.
7) Should close the store and not serve any more customers after all the shifts. V
"""
from typing import List

import pytest
from core import (
    CashRegisterFactory,
    Customer,
    Discount,
    ICashRegisterFactory,
    IDataBase,
    StoreService,
)
from hypothesis import given
from hypothesis.strategies import integers
from infra import CardPayment, CashPayment, ItemEntry, MemoryDataBase
from strategies import cash_register_factories, data_bases, positive_integers
from stubs import StubReportPrinter


@pytest.fixture
def memory_data_base() -> MemoryDataBase:
    """Returns an initialized in-memory data base."""
    return MemoryDataBase(
        items=[
            ItemEntry("Bread", price=1.4),
            ItemEntry("Milk", price=4.0),
            ItemEntry("Mustard", price=0.4),
        ],
        discounts=[
            Discount(
                related_item="Bread",
                required_items={"Bread": 2},
                reduction_percentage=10,
            ),
            Discount(
                related_item="Bread",
                required_items={"Bread": 1, "Milk": 1},
                reduction_percentage=15,
            ),
            Discount(
                related_item="Milk",
                required_items={"Mustard": 1},
                reduction_percentage=50,
            ),
        ],
    )


@pytest.fixture
def customers() -> List[Customer]:
    """Returns a list of customers with a chosen collection of items."""
    return [
        Customer(
            id="Customer 1",
            item_list=["Bread", "Bread", "Mustard"],
            payment=CashPayment(),
        ),
        Customer(
            id="Customer 2",
            item_list=["Milk", "Milk", "Milk"],
            payment=CardPayment(),
        ),
        Customer(
            id="Customer 3",
            item_list=["Bread", "Milk", "Mustard"],
            payment=CashPayment(),
        ),
    ]


@given(
    cash_registers=positive_integers(),
    daily_shifts=positive_integers(),
    cash_register_factory=cash_register_factories(),
    data_base=data_bases(),
)
def test_should_create_store_given(
    cash_registers: int,
    daily_shifts: int,
    cash_register_factory: ICashRegisterFactory,
    data_base: IDataBase,
) -> None:
    StoreService(
        cash_registers=cash_registers,
        daily_shifts=daily_shifts,
        cash_register_factory=cash_register_factory,
        data_base=data_base,
    )


@given(
    cash_registers=positive_integers(),
    daily_shifts=positive_integers(),
    cash_register_factory=cash_register_factories(),
    data_base=data_bases(),
)
def test_should_open_the_store_given(
    cash_registers: int,
    daily_shifts: int,
    cash_register_factory: ICashRegisterFactory,
    data_base: IDataBase,
) -> None:
    data_base.add_sold_item("Item 1", total_price=5.0)
    data_base.add_sold_item("Item 2", total_price=15.0, count=50)

    StoreService(
        cash_registers=cash_registers,
        daily_shifts=daily_shifts,
        cash_register_factory=cash_register_factory,
        data_base=data_base,
    ).open_store()

    assert data_base.get_distinct_sold_items() == ["Item 1", "Item 2"]
    assert data_base.get_sold_item_count("Item 1") == 1
    assert data_base.get_sold_item_count("Item 2") == 50
    assert data_base.get_total_revenue() == 20.0

    StoreService(
        cash_registers=cash_registers,
        daily_shifts=daily_shifts,
        cash_register_factory=cash_register_factory,
        data_base=data_base,
    ).open_store(clear_data=True)

    assert data_base.get_distinct_sold_items() == []
    assert data_base.get_total_revenue() == 0


@given(
    cash_registers=positive_integers(),
    daily_shifts=integers(min_value=1, max_value=10),
    cash_register_factory=cash_register_factories(),
    data_base=data_bases(),
)
def test_should_reopen_a_closed_store_given(
    cash_registers: int,
    daily_shifts: int,
    cash_register_factory: ICashRegisterFactory,
    data_base: IDataBase,
) -> None:

    store = StoreService(
        cash_registers=cash_registers,
        daily_shifts=daily_shifts,
        cash_register_factory=cash_register_factory,
        data_base=data_base,
    )

    store.open_store()

    for _ in range(daily_shifts):
        store.generate_shift_report()

    store.open_store()


def test_should_serve_customer(memory_data_base: MemoryDataBase) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=3,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )
    customer = Customer(
        id="Customer 1",
        item_list=["Bread", "Bread", "Mustard"],
        payment=CashPayment(),
    )

    store.open_store(clear_data=True)
    store.serve_customer(customer)

    assert memory_data_base.get_sold_item_count("Bread") == 2
    assert memory_data_base.get_sold_item_count("Mustard") == 1
    assert sorted(memory_data_base.get_distinct_sold_items()) == ["Bread", "Mustard"]
    assert memory_data_base.get_total_revenue() == 2 * 1.4 * (100 - 10) / 100 + 0.4


def test_should_serve_multiple_customers(memory_data_base: MemoryDataBase) -> None:
    store = StoreService(
        cash_registers=5,
        daily_shifts=3,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )
    customers = [
        Customer(
            id="Customer 1",
            item_list=["Bread", "Bread", "Mustard"],
            payment=CashPayment(),
        ),
        Customer(
            id="Customer 2",
            item_list=["Milk", "Milk", "Milk"],
            payment=CardPayment(),
        ),
    ]

    store.open_store(clear_data=True)

    for customer in customers:
        store.serve_customer(customer)

    assert memory_data_base.get_sold_item_count("Milk") == 3
    assert memory_data_base.get_sold_item_count("Bread") == 2
    assert memory_data_base.get_sold_item_count("Mustard") == 1
    assert sorted(memory_data_base.get_distinct_sold_items()) == [
        "Bread",
        "Milk",
        "Mustard",
    ]
    assert (
        memory_data_base.get_total_revenue()
        == 2 * 1.4 * (100 - 10) / 100 + 0.4 + 3 * 4.0
    )


def test_should_create_report_of_store_state(memory_data_base: MemoryDataBase) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=1,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )
    customers = [
        Customer(
            id="Customer 2",
            item_list=["Milk", "Milk", "Milk"],
            payment=CardPayment(),
        ),
    ]

    store.open_store(clear_data=True)

    for customer in customers:
        store.serve_customer(customer)

    printer = StubReportPrinter()
    printer.expect(
        item_counts={"Milk": 3},
        distinct_items=["Milk"],
        revenue=3 * 4.0,
    )

    store.generate_report(report_printer=printer)


def test_should_create_end_of_shift_report(
    memory_data_base: MemoryDataBase, customers: List[Customer]
) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=1,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )

    store.open_store(clear_data=True)

    for customer in customers:
        store.serve_customer(customer)

    customer_1_revenue = 2 * 1.4 * (100 - 10) / 100 + 0.4
    customer_2_revenue = 3 * 4.0
    customer_3_revenue = 1 * 1.4 * (100 - 15) / 100 + 0.4 + 1 * 4.0 * (100 - 50) / 100

    printer = StubReportPrinter()
    printer.expect(
        item_counts={"Bread": 3, "Mustard": 2, "Milk": 4},
        distinct_items=["Bread", "Milk", "Mustard"],
        revenue=customer_1_revenue + customer_2_revenue + customer_3_revenue,
    )

    store.generate_shift_report(report_printer=printer)

    assert memory_data_base.get_distinct_sold_items() == []
    assert memory_data_base.get_total_revenue() == 0


def test_should_create_end_of_shift_report_without_printer(
    memory_data_base: MemoryDataBase, customers: List[Customer]
) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=1,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )

    store.open_store(clear_data=True)

    for customer in customers:
        store.serve_customer(customer)

    store.generate_shift_report()

    assert memory_data_base.get_distinct_sold_items() == []
    assert memory_data_base.get_total_revenue() == 0


def test_should_have_shifts_remaining(
    memory_data_base: MemoryDataBase, customers: List[Customer]
) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=1,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )

    store.open_store(clear_data=True)

    assert store.has_shifts_remaining() is True


def test_should_not_have_shifts_remaining(
    memory_data_base: MemoryDataBase, customers: List[Customer]
) -> None:
    store = StoreService(
        cash_registers=1,
        daily_shifts=2,
        cash_register_factory=CashRegisterFactory(),
        data_base=memory_data_base,
    )

    store.open_store(clear_data=True)

    store.generate_shift_report()
    store.generate_shift_report()

    assert store.has_shifts_remaining() is False
